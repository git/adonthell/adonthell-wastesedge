#!/bin/sh

adonthell_exe="adonthell-0.3"

# -- check arg
if test "x$1" = "x" ; then
  echo "Usage: $0 <path/to/Adonthell.App>"
  exit 1
fi

# -- remove traiing slash, if present
bundle=${1%/}

if test ! -f $bundle"/Contents/MacOS/$adonthell_exe" ; then
  echo "Error: $bundle is not the expected App bundle"
  exit 1
fi

# -- check working directory
if [ ! -f "background.png" ]; then
  echo "This script must be run in the wastesedge-0.3.x/osx directory"
  exit 1
fi

# -- check dmgbuild
if [ ! -x "$(command -v dmgbuild)" ]; then
  echo "This script requires dmgbuild installed and in your PATH"
  echo "See https://pypi.python.org/pypi/dmgbuild"
  exit 1
fi

# -- get adonthell version
adonthell_ver=`$bundle/Contents/MacOS/$adonthell_exe -v`

# -- codesign Adonthell.App
if [ ! -z "$CODESIGN_IDENTITY" ]; then
  echo "Signing $bundle as $CODESIGN_IDENTITY"
  codesign -s "$CODESIGN_IDENTITY" --entitlements sandbox.plist --force --deep $bundle
  cat > $bundle/Contents/Resources/container-migration.plist <<EOF
<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE plist PUBLIC "-//Apple//DTD PLIST 1.0//EN" "http://www.apple.com/DTDs/PropertyList-1.0.dtd">
<plist version="1.0">
<dict>
        <key>Move</key>
        <array>
                <array>
                        <string>\${ApplicationSupport}/Adonthell</string>
                        <string>\${ApplicationSupport}/Adonthell</string>
                </array>
        </array>
</dict>
</plist>
EOF
else
  echo "CODESIGN_IDENTITY not defined. Will not sign Adonthell.app"
fi

echo "Creating wastesedge-osx-x86_64-$adonthell_ver.dmg"

# -- clean up any __pycache__ directories before collecting package contents
find $bundle -name __pycache__ -execdir rm -rf __pycache__ \;

dmgbuild -s settings.py -Dapp=$bundle "Adonthell - Waste's Edge" wastesedge-osx-x86_64-$adonthell_ver.dmg

# -- codesign diskimage (requires OSX 10.11.4 or later)
if [ ! -z "$CODESIGN_IDENTITY" ]; then
  echo "Signing wastesedge-osx-x86_64-$adonthell_ver.dmg as $CODESIGN_IDENTITY"
  codesign -s "$CODESIGN_IDENTITY" wastesedge-osx-x86_64-$adonthell_ver.dmg
fi
