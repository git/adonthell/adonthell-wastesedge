#!/bin/sh

adonthell_exe="adonthell-0.3.exe"

# -- check arg
if test "x$1" = "x" ; then
  echo "Usage: $0 <path/to/Adonthell>"
  exit 1
fi

if test ! -f $1"/bin/$adonthell_exe" ; then
  echo "Error: $1 is not the expected adonthell package"
  exit 1
fi

# -- determine installer architecture (x64 or x86)
arch=`file $1"/bin/$adonthell_exe" | grep -o -e x86-64 -e 80386`
case $arch in
	x86-64)
		arch="x64"
		;;
	80386)
		arch="x86"
		;;
	*)
		echo "Unknown platform $arch"
		exit 1
		;;
esac

# -- check working directory
if [ ! -f "wastesedge.wxs" ]; then
  echo "This script must be run in the wastesedge-0.3.x/win32 directory"
  exit 1
fi

# -- check Wix Toolkit
if [ ! -x "$(command -v heat)" ]; then
  echo "This script requires the WiX toolset installed and in your PATH"
  echo "See http://wixtoolset.org/"
  exit 1
fi

# -- get adonthell version
adonthell_ver=`$1/bin/$adonthell_exe -v`

# -- codesign Adonthell exe
if [ -x "$(command -v signtool)" ]; then
  echo "Codesigning $adonthell_exe"
  MSYS2_ARG_CONV_EXCL=/t signtool.exe sign /t http://timestamp.comodoca.com/authenticode $1/bin/$adonthell_exe
fi

echo "Creating wastesedge-$adonthell_ver-$arch.msi"

# -- check if the User Manual.pdf is present
if [ ! -f "../User\ Manual.pdf" ]; then
	manual=1
else
	echo "PDF Manual not present. Will not be included in installer."
	manual=0
fi

# -- clean up any __pycache__ directories before collecting package contents
find $1 -name __pycache__ -execdir rm -rf __pycache__ \;

# -- collect package contents
heat dir "$1" -cg WastesedgePackage -dr INSTALLDIR -gg -sfrag -srd -sw5150 -template fragment -out package.wxs
if [ $? -ne 0 ]; then
   exit 1
fi

# -- compile
candle -arch $arch -dVersion="$adonthell_ver" -dArch="$arch" -dHaveManual="$manual" wastesedge.wxs package.wxs
if [ $? -ne 0 ]; then
   exit 1
fi

# -- link
light -b $1 -sw1076 -ext WixUIExtension wastesedge.wixobj package.wixobj -o wastesedge-$adonthell_ver-$arch.msi
if [ $? -ne 0 ]; then
   exit 1
fi

# -- codesign installer
if [ -x "$(command -v signtool)" ]; then
  echo "Codesigning wastesedge-$adonthell_ver-$arch.msi"
  MSYS2_ARG_CONV_EXCL="/d;/t" signtool.exe sign /t http://timestamp.comodoca.com/authenticode /d "Adonthell - Waste's Edge v$adonthell_ver ($arch)" wastesedge-$adonthell_ver-$arch.msi
fi

# -- cleanup
rm wastesedge-$adonthell_ver-$arch.wixpdb
rm wastesedge.wixobj
rm package.wixobj
rm package.wxs