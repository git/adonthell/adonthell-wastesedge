#
#  $Id$
#
#  (C) Copyright 2016 Kai Sterker <kai.sterker@gmail.com>
#  Part of the Adonthell Project <http://adonthell.nongnu.org>
#
#  Adonthell is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  Adonthell is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with Adonthell.  If not, see <http://www.gnu.org/licenses/>.
#

import adonthell

# -- pygettext support
def N_(message): return message
def _(message): return adonthell.nls_translate (message)

# -- GUI for displaying in-game achievements
class achievement_screen (adonthell.win_container):
        
    # -- dictionary of achievements present in the game
    #    with their unique id as key
    achievements = { \
        1: (N_("Sommelier"), N_("You know a good drop when you taste it")), \
        2: (N_("Conversationalist"), N_("None will escape your silver tongue")), \
        3: (N_("Keeper of secrets"), N_("Embarrassing confessions are safe with you")), \
        4: (N_("Revealer of truth"), N_("Under your vigilant eye, no wrongdoing stays hidden")), \
        5: (N_("Trusty servant"), N_("As always, you keep your mistress out of harm's way")), \
        6: (N_("Yetin acolyte"), N_("You walk the path of the yetin spirit")), \
        7: (N_("Master detective"), N_("Your inquisitive powers lead you to the right conclusions")), \
        8: (N_("Gem-bearer"), N_("What is thought to be lost, you already discovered")), \
        9: (N_("Paragon of virtue"), N_("Neither lie nor deceit parts your lips")), \
        255: (N_("Leet haxor"), N_("Your hex editing skillz are unparalleled ... or your disk broken")) }
    
    # -- Constructor
    def __init__(self):    
        adonthell.win_container.__init__(self)

        self.scroll_direction = -1
        
        # -- get fonts and theme
        self.font1 = adonthell.win_manager_get_font ("yellow")
        self.font2 = adonthell.win_manager_get_font ("original")
        self.theme = adonthell.win_manager_get_theme ("original")
        
        self.move (19, 15)
        self.resize (282, 216)
        
        # -- list of achievements
        self.achievement_list = adonthell.win_select()
        self.achievement_list.move (11, 20)
        self.achievement_list.resize (260, 186)
        self.achievement_list.set_border (self.theme)
        self.achievement_list.set_visible_border (True)
        self.achievement_list.set_background (self.theme)
        self.achievement_list.set_visible_background (True)
        self.achievement_list.set_trans_background (True)
        self.achievement_list.set_scrollbar (self.theme);
        self.achievement_list.set_visible_scrollbar (True);
        self.achievement_list.set_space_between_object(6)
        self.achievement_list.set_space_with_border(6)
        self.achievement_list.set_layout (adonthell.win_container_LIST_LAYOUT)
        self.achievement_list.set_mode (adonthell.win_select_MODE_BORDER)
        self.achievement_list.py_signal_connect (self.page_up, adonthell.win_event_PREVIOUS)
        self.achievement_list.py_signal_connect (self.page_down, adonthell.win_event_NEXT)
        self.achievement_list.thisown = 0

        # -- activate the list
        self.achievement_list.set_activate (True)
    
        # -- give focus to the list
        self.add(self.achievement_list)
        self.set_focus_object (self.achievement_list)
          
        # -- add achievements
        num_achievements = adonthell.achievements.num_achievements()
        for i in range(num_achievements):
            id = adonthell.achievements.achievement_id(i)
            unlocked = adonthell.achievements.is_unlocked(i)
            self.achievement_list.add(self.make_item(id, unlocked))

        # -- add title
        title = adonthell.win_label()
        title.set_form (adonthell.label_AUTO_SIZE)
        title.set_font (self.font2)
        title.set_text (_("Achievements") + " [%s / %s]" % (adonthell.achievements.num_unlocked(), num_achievements))
        title.move ((self.length() - title.length()) // 2, 0)       
        title.pack ()
        title.thisown = 0

        self.add(title)
                    
        # -- set everything visible
        self.set_visible_all (True)
        self.set_activate (True)

        self.py_signal_connect (self.on_update, adonthell.win_event_UPDATE)

    def make_item(self, id, unlocked):
        # -- get title and text of the given achievement id
        data = achievement_screen.achievements.get(id)
        if data == None: return
        
        iconId = 0
        if unlocked: iconId = id

        iconpath = adonthell.game.find_file("gfx/achievements/%s.pnm" % (iconId))
        
        container = adonthell.win_container()
        container.thisown = 0

        icon = adonthell.win_image ()
        icon.load_pnm (iconpath); 
        icon.move (8, 3);
        icon.set_border (self.theme, adonthell.win_border_MINI)
        icon.set_visible_border (True)
        icon.pack()
        icon.thisown = 0

        label1 = adonthell.win_label()
        label1.resize (176, 18)
        label1.move (64, 0)
        if unlocked:
            label1.set_font (self.font1)
        else:
            label1.set_font (self.font2)
        label1.set_form (adonthell.label_AUTO_SIZE)
        label1.set_text (adonthell.nls_translate (data[0]))
        label1.pack ()
        label1.set_brightness(not unlocked)
        label1.thisown = 0

        label2 = adonthell.win_label()
        label2.resize (176, 36)
        label2.move (64, 17)
        label2.set_font (self.font2)
        label2.set_text (adonthell.nls_translate (data[1]))
        label2.pack ()
        label2.thisown = 0

        container.add(icon)
        container.add(label1)
        container.add(label2)
        container.resize (250, 54)        
        container.set_visible_all (True)
        
        return container

    # -- callback for custom updating
    def on_update (self):
        # -- pressing ESC will close the dialog
        if adonthell.input_has_been_pushed (adonthell.SDLK_ESCAPE):
            adonthell.gamedata_engine ().main_quit ()

    def page_up (self):
        # -- do a page-wise scrolling of the achievment list
        pos = self.achievement_list.get_selected_position()
        if self.scroll_direction == 1:
            self.achievement_list.set_default_position(pos - 3)
        self.scroll_direction = -1
        
    def page_down (self):
        # -- do a page-wise scrolling of the achievment list
        pos = self.achievement_list.get_selected_position()
        if self.scroll_direction == -1:
            self.achievement_list.set_default_position(pos + 1)
        self.scroll_direction = 1


# -- GUI for displaying a popup when an achievement unlocks
class achievement_popup (adonthell.win_container):
    
    # -- position where the popup should appear, in case multiple achievements unlock at once
    pos_y = [ 5 ]
    
    # -- Constructor
    def __init__(self, achievement):
        adonthell.win_container.__init__(self)

        # -- check that the achievement data exists
        data = achievement_screen.achievements.get(achievement)
        if data == None: return
        
        # -- number of cycles the popup remains open
        self.time_remaining = 250

        # -- get font and theme
        self.font = adonthell.win_manager_get_font ("yellow")
        self.theme = adonthell.win_manager_get_theme ("original")
        
        pos_y = achievement_popup.pos_y[-1]
        self.move (190, pos_y)
        self.resize (125, 28)
        
        achievement_popup.pos_y.append(pos_y + 34)
        
        self.set_border (self.theme, adonthell.win_border_MINI)
        self.set_visible_border (True)
        self.set_background (self.theme)
        self.set_visible_background (True)
        self.set_trans_background (True)
        self.thisown = 0

        iconpath = adonthell.game.find_file("gfx/achievements/%s.pnm" % (achievement))

        tmp_image = adonthell.image ()
        tmp_image.load_pnm (iconpath)
        
        icon = adonthell.win_image ()
        icon.resize (24, 24)
        icon.move (3, 2);
        icon.zoom (tmp_image)
        icon.pack()
        icon.thisown = 0

        label = adonthell.win_label()
        label.resize (90, 24)
        label.move (30, 2)
        label.set_font (self.font)
        label.set_text (adonthell.nls_translate(data[0]))
        label.pack ()
        label.thisown = 0

        self.add (icon)
        self.add (label)

        self.set_visible_all (True)
    
        self.py_signal_connect (self.on_update, adonthell.win_event_UPDATE)
        self.py_signal_connect (self.on_destroy, adonthell.win_event_DESTROY)
    
    def on_update(self):
        # -- count down how long the popup remains open
        self.time_remaining = self.time_remaining - 1
        
    def on_destroy(self):
        # -- close the popup when the time arrives
        if self.time_remaining == 0:
            achievement_popup.pos_y.pop(1)
            return False
        
        return True
